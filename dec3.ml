(*--- Day 3: Spiral Memory ---*)

(*You come across an experimental new kind of memory stored on an infinite*)
(*two-dimensional grid.*)

(*Each square on the grid is allocated in a spiral pattern starting at a location*)
(*marked 1 and then counting up while spiraling outward. For example, the first*)
(*few squares are allocated like this:*)

(*17  16  15  14  13*)
(*18   5   4   3  12*)
(*19   6   1   2  11*)
(*20   7   8   9  10*)
(*21  22  23---> ...*)

(*While this is very space-efficient (no squares are skipped), requested data*)
(*must be carried back to square 1 (the location of the only access port for this*)
(*memory system) by programs that can only move up, down, left, or right. They*)
(*always take the shortest path: the Manhattan Distance between the location of*)
(*the data and square 1.*)

(*For example:*)

    (*Data from square 1 is carried 0 steps, since it's at the access port.*)
    (*Data from square 12 is carried 3 steps, such as: down, left, left.*)
    (*Data from square 23 is carried only 2 steps: up twice.*)
    (*Data from square 1024 must be carried 31 steps.*)

(*How many steps are required to carry the data from the square identified in*)
(*your puzzle input all the way to the access port?*)

(*Your puzzle input is 361527.*)

(* N:
    * Increases by 8
    * 1 -> 4 -> 15 -> 34 -> 61 ->
    *   3    11    19    37    45
*)
(* NE:
    * Increases by 8
    * 1 -> 3 -> 13 -> 31 -> 57 ->
    *   2    10    18    26    34
*)
(* NW:
    * Increases by 8
    * 1 -> 5 -> 17 -> 37 -> 65 ->
    *   4    12    20    28    36
*)

(* W:
    * Increases by 8
    * 1 -> 6 -> 19 -> 40 -> 69 ->
    *   5    13    21    29    37
*)
(* SW:
    * Increases by 8
    * 1 -> 7 -> 21 -> 43 -> 73 ->
    *   6    14    22    30    38
*)

open Batteries

let lol = [
    [65; 64; 63; 62; 61; 60; 59; 58; 57; 90;];
    [66; 37; 36; 35; 34; 33; 32; 31; 56; 89;];
    [67; 38; 17; 16; 15; 14; 13; 30; 55; 88;];
    [68; 39; 18;  5;  4;  3; 12; 29; 54; 87;];
    [69; 40; 19;  6;  1;  2; 11; 28; 53; 86;];
    [70; 41; 20;  7;  8;  9; 10; 27; 52; 85;];
    [71; 42; 21; 22; 23; 24; 25; 26; 51; 84;];
    [72; 43; 44; 45; 46; 47; 48; 49; 50; 83;];
    [73; 74; 75; 76; 77; 78; 79; 80; 81; 82;];
]

let () =
    print_string "WAT?"
